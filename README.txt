#MSCH-G300 First Unity Game

Hello!

This project is a 2D puzzle platformer that I have dubbed Trippy.

The goal is to unlock the three doors. 
Upon unlocking the three doors you are free to escape the confines of the level... 
and potentially achieve nirvana by accepting the state of falling endlessly in a gray void.

There is no lose state, so if you get stuck I would reccomend reloading the scene.

Controls:
Move Horizontally: Left/Right arrow keys and AD
Jump: Space bar

Enjoy!