﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
RedPickup is a collision handler that doubles the jumpTakeoffSpeed for 5 seconds. 
*/

public class RedPickup : MonoBehaviour
{
    private PlayerPlatformerController playerScipt;
    private BoxCollider2D boundingBox;
    private SpriteRenderer currentSprite;

    void Start() {
        playerScipt = FindObjectOfType<PlayerPlatformerController>();
        boundingBox = GetComponent<BoxCollider2D>();
        currentSprite = GetComponent<SpriteRenderer>();
    }
    void OnTriggerEnter2D(Collider2D other){
        StartCoroutine(Wait());
    }

    IEnumerator Wait() {
        //Disallow collisions so OnTriggerEnter2D is not called mulitple times
        boundingBox.enabled = false;
        currentSprite.enabled = false;
        Debug.Log("before");
        playerScipt.jumpTakeOffSpeed = 14;
        //jump height doubled
        yield return new WaitForSeconds(5);
        playerScipt.jumpTakeOffSpeed = 7;
        //jump height normal
        Debug.Log("after");

        //"respawn" RedPickup
        boundingBox.enabled = true;
        currentSprite.enabled = true;
    }
}
