﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
OnOffPlatformSwitch  is attached to the OnOffPlatforms and is only called when a button is pressed. 

Upon activation the BoxCollider2D component is enabled and the sprite is changed to represent a solid block.
*/


public class OnOffPlatformSwitch : MonoBehaviour
{
    public Sprite filledBlock;
    public Sprite emptyBlock;

    //keeps track of block state
    public bool isOn;

    SpriteRenderer[] sprites;
    BoxCollider2D[] boundingBox;

    void Start() {
        sprites = GetComponentsInChildren<SpriteRenderer>();
        boundingBox = GetComponentsInChildren<BoxCollider2D>();

    }


//called by PushButton.OnTriggerEnter2D(Collider2D other)
    public void switchBlock(){
        foreach(SpriteRenderer currentSprite in sprites){
            //switch sprite and toggle box
            if(isOn){
                currentSprite.sprite = emptyBlock;
            } else {
                currentSprite.sprite = filledBlock;
            }
        }

        foreach(BoxCollider2D collider in boundingBox){
            collider.enabled = !collider.enabled;
        }
        isOn = !isOn;
    }
}
