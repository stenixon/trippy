﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController : PhysicsObject
{
	/* 
	https://learn.unity.com/tutorial/live-session-2d-platformer-character-controller#

	Source for code.

	PlayerPlatformerController computes most of the Player Physics 


	*/

	//jumpTakeOffSpeed and maxSpeed are mutable variables, these will be changed by external scripts (e.g. RedPickup)
	public float jumpTakeOffSpeed = 7;
	public float maxSpeed = 7;
	

	//keycount is a mutable variable keeps track of how many keys have been collected
	private bool greenkey;
	private bool redkey;
	private bool bluekey;
	
	private SpriteRenderer spriteRenderer;
	private Animator animator;

	
	
    void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
	}

	//main input control, handles left/right and jump inputs (unchanged from tutorial)
	protected override void ComputeVelocity() {
		Vector2 move = Vector2.zero;
		
		move.x = Input.GetAxis("Horizontal");
		
		if(Input.GetButtonDown("Jump") && grounded) {
			velocity.y = jumpTakeOffSpeed;
			
		} else if (Input.GetButtonUp("Jump")){
			if (velocity.y > 0) velocity.y = velocity.y * 0.5f;
		}
		

		bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f)) ;
		if(flipSprite){
			spriteRenderer.flipX = !spriteRenderer.flipX;
		}
		
		animator.SetBool("grounded", grounded);
		animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
		
		targetVelocity = move * maxSpeed;
	}
	
	/* 
	All methods below handle the Key/Door mechanic
	*/

	public void KeyGet(keyColor color){
		switch(color){
			case keyColor.Green:
				greenkey = true;
				break;
			case keyColor.Red:
				Debug.Log("Helppppp");
				redkey = true;
				break;
			case keyColor.Blue:
				bluekey = true;
				break;

		}
	}

//@param keyColor passes the color of the Door that the player has collided with
//@return bool determining whether the player has a key of the passed color
	public bool DoorOpen(keyColor doorColor){
		switch(doorColor){
			case keyColor.Green:
				if(greenkey) return true;
				break;
			case keyColor.Red:
				if(redkey) return true;
				break;
			case keyColor.Blue:
				if(bluekey) return true;
				break;
		}
		return false;
	}
	
}
