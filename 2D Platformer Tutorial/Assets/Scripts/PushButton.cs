﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
PushButton is a collision handler that toggles the on/off platforms

Upon collison the button sprite is switched and the BoxCollider2D  of the on/off platforms are enabled.


*/
public class PushButton : MonoBehaviour
{
    public Sprite pushedButton;
    public Sprite unpushedButton;
    //isPushed keeps track of button state (On/Off)
    private bool isPushed;
    public GameObject platSwitch;
    private OnOffPlatformSwitch platScript;
    private BoxCollider2D buttonBounds;
    private SpriteRenderer currentSprite;
    void Start() {
        isPushed = false;
        platScript = platSwitch.GetComponent<OnOffPlatformSwitch>();
        buttonBounds = GetComponent<BoxCollider2D>();
        currentSprite = GetComponent<SpriteRenderer>();
        
    }
    void OnTriggerEnter2D(Collider2D other) {
        if(isPushed){
            currentSprite.sprite = unpushedButton;
        } else {
            currentSprite.sprite = pushedButton;
        }
        isPushed = !isPushed;
        platScript.switchBlock();
    }
}
