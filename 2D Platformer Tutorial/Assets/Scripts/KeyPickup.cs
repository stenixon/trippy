﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* 
KeyPickup is a collision handler that adds one to the players keycount and destroys itself
*/
public enum keyColor {Red, Green, Blue};

public class KeyPickup : MonoBehaviour
{

	public GameObject self;	
	public keyColor thisKeyColor;

	private PlayerPlatformerController playerScript;
	
	void Start() {
		
		playerScript = FindObjectOfType<PlayerPlatformerController>();
	}
	
	//handles collisions with objects: if player collides, KeyGet will add the key to the player "inventory" and Destroy Key GameObject.
	void OnTriggerEnter2D(Collider2D other){
		
		if(other.gameObject.CompareTag("Player")){
			playerScript.KeyGet(thisKeyColor);
			Destroy(self);
		}
 
	}

}
