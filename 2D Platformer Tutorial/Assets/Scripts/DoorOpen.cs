﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
DoorOpen is a collision handler that checks whether the player has any keys, and deletes door if true.
	Otherwise the door remains as a static colldier
*/

public class DoorOpen : MonoBehaviour
{
	public GameObject self;
	private PlayerPlatformerController playerScript;
	private SpriteRenderer[] currentSprites;
	private BoxCollider2D boundingBox;
	public keyColor doorColor;
	
	void Start() {
		playerScript = FindObjectOfType<PlayerPlatformerController>();
		boundingBox = GetComponent<BoxCollider2D>();
		currentSprites = GetComponentsInChildren<SpriteRenderer>();
	}
	

	//Handles player collision with door

	void OnCollisionEnter2D(Collision2D other){
			if(other.gameObject.CompareTag("Player")){
				if(playerScript.DoorOpen(doorColor)){
					Destroy(self);
			}
		}
 
	}

}
